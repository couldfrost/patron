import Infanteria from './Infanteria';
import Arqueria from './Arqueria';


class Gerrero {
    
        constructor(tipo) {

                if(tipo==="infanteria")
                {
                this.calculardaño=new Infanteria();
                }
                if(tipo==="arqueria")
                {
                this.calculardaño=new Arqueria();
                }
            
        }
        obtenerDaño(){
                return this.calculardaño.obtenerDaño();
        }

        
    
}
    
module.exports = Gerrero;