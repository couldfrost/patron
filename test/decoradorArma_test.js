let expect = require('chai').expect;
import Decoradorarma from "../DecoradorArma"
import Guerrero from "../Gerrero"

describe('Decorar', function() {

    it('Deberia retornar el daño del respectivo guerrero', function() {
        let guerrero=new Guerrero("infanteria")
        let decorar=new Decoradorarma("fuego",guerrero);
        expect(decorar.obtenerDaño()).equal(25);
    });
    /*
    it('Deberia retornar el daño del respectivo guerrero', function() {
        let decorar=new Decoradorarma("metal");
        expect(decorar.obtenerDaño()).equal(10);
    });
    it('Deberia retornar el daño del respectivo guerrero', function() {
        let decorar=new Decoradorarma("madera");
        expect(decorar.obtenerDaño()).equal(5);
    });
    */
});