let expect = require('chai').expect;
import Guerrero from "../Gerrero"


describe('Guerrero', function() {

    it('Deberia retornar el daño del respectivo guerrero', function() {
        let guerrero=new Guerrero("infanteria");
        expect(guerrero.obtenerDaño()).equal(10);
    });
    it('Deberia retornar el daño del respectivo guerrero', function() {
        let guerrero=new Guerrero("arqueria");
        expect(guerrero.obtenerDaño()).equal(5);
    });
});