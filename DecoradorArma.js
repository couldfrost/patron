import Fuego from './Fuego';
import Metal from './Metal';
import Madera from './Madera';
class DecoradorArma {
    
        constructor(tipo,guerrero) {
             if(tipo==="fuego")
             {
                this.calculardaño=new Fuego(guerrero);
             }
             if(tipo==="metal")
             {
                this.calculardaño=new Metal(guerrero);
             }
             if(tipo=="madera"){
                this.calculardaño=new Madera(guerrero);
             }
            
        }
        obtenerDaño(){
              return this.calculardaño.obtenerDaño();
        }
        

        
    
}
    
module.exports = DecoradorArma;