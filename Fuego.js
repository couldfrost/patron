class Fuego {
    
        constructor(guerrero) {
            this.guerrero=guerrero
            this.dañofuego=15;
        }
        cambiar(){
            this.guerrero.cambiarDaño(this.obtenerDaño());
        }
        obtenerDaño(){
            return this.dañofuego+this.guerrero.obtenerDaño();
        }

        
    
}
    
module.exports = Fuego;